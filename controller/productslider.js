const Slide = require("../models/ProductSlider")
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.createProductSlide = (req, res) => {
  if (!req.user.isAdmin) {
    return Promise.reject("Only administrators can create products.");
  }

  const newProductSlide = new Slide({
    isAdmin: req.body.isAdmin || false,
    imgURL: req.body.imgURL
  });

  return newProductSlide.save()
    .then(() => {
      return res.send(true); 
    })
    .catch((error) => {
      console.error(error);
      return res.send(false); 
    });
};

//Retrieve all products
module.exports.getAllProductsSlide = (req,res) =>{
	return Product.find({}).then(result=>{
		return res.send(result)
	})
}

// //Update Product information (Admin only)

module.exports.updateProductSlide = (req,res)=>{

	let updatedProduct = {
		name: req.body.name, 
        description: req.body.description,
        imgURL: req.body.imgURL,
        price: req.body.price,
        isActive: req.body.isActive, 
        purchasedOn: req.body.purchasedOn,
        userOrders: req.body.userOrders 
	}

	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error)=>{
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
}


//Archive Product (Admin only)
module.exports.archiveProductSlide = (req, res) => {
    Product.findById(req.params.productId).then(product => {
        if (product.isActive === true) {
            product.isActive = false;
            return product.save().then(updatedProduct => {
                if (updatedProduct) {
                    return res.send(true);
                } else {
                    return res.send(false);
                }
            });
        }
    }).catch(err => {
        console.error(err);
        return res.send(false);
    });
};

//Activate Product (Admin only)
module.exports.activateProductSlide = (req, res) => {
    Product.findById(req.params.productId).then(product => {
        if (product.isActive === false) {
            product.isActive = true;
            return product.save().then(updatedProduct => {
                if (updatedProduct) {
                    return res.send(true);
                } else {
                    return res.send(false);
                }
            });
        }
    }).catch(err => {
        console.error(err);
        return res.send(false);
    });
};

