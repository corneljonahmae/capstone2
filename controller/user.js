const Product = require("../models/Product")
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Registration
module.exports.registerUser = (req, res) => {
  bcrypt.hash(req.body.password, 10, (hashError, hashedPassword) => {
    if (hashError) {
      console.error(hashError);
      return res.send(false);
    }

    const newUser = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      mobileNumber: req.body.mobileNumber,
      email: req.body.email,
      password: hashedPassword,
      isAdmin: req.body.isAdmin
    });

    newUser.save()
      .then(savedUser => {
        return res.send(true); 
      })
      .catch(saveError => {
        console.error(saveError);
        return res.send(false); 
      });
  });
};

//Authentication
module.exports.loginUser = (req,res)=>{
	return User.findOne({email:req.body.email}).then(result=>{
		if (result === null){
			return res.send(false)
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)
			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}
			
			else {
				return res.send(false);
			}
		}
	})
	.catch(err=>res.send(err))
}

// Retrieve user details
/*
    Steps:
    1. Find the document in the database using the user's ID
    2. Reassign the password of the returned document to an empty string
    3. Return the result back to the frontend
*/
module.exports.getProfile = (req, res) => {

  return User.findById(req.user.id)
  .then(result => {
      result.password = "";
      return res.send(result);
  })
  .catch(err => res.send(err))

};

module.exports.createOrder = async (req, res) => {
  const generateOrderID = () => {
    const timestamp = new Date().getTime(); // Current timestamp
    const randomPart = Math.floor(Math.random() * 10000); // Random 4-digit number
    const prefix = "ORD"; // Prefix to indicate it's an order ID
    const orderID = `${prefix}-${timestamp}-${randomPart}`;
    return orderID;
  };

  const orderID = generateOrderID();
	// to check in the terminal the user id and the productId
	console.log(req.user.id);
	console.log(req.body.productId);

	// checks if the user is an admin and deny the enrollment
	if(req.user.isAdmin){
		return res.send("Action Forbidden");
	}

	// Creates an "isUserUpdated" variabe and returns true upon successful update otherwise returns error
	let isUserUpdated = await User.findById(req.user.id).then(user => {
		
		//Add the product in an object and push that object into the user's "orderedProducts" array
		let newOrderedProducts = {
      products: [
        {
          productId: req.body.productId,
          productName: req.body.productName,
          quantity: "1"
        },
      ],
      totalAmount: req.body.price, 
      purchasedOn: new Date()
		}

		// Add the product in the "orderedProducts" array
		user.orderedProduct.push(newOrderedProducts);

		// Save the updated user and return true if successful or the error message if failed.
		return user.save().then(user => true).catch(err => err.message);


	});

	// Checks if there are error in updating the user
	if(isUserUpdated !== true){
		return res.send({ message: isUserUpdated});
	}

	let isProductUpdated = await Product.findById(req.body.productId).then(product => {

		let customer = {
			userId: req.user.id,
      orderId: orderID  
		}
    console.log(customer)

    if (product && product.userOrders) {
      // Access the userOrders property here
      product.userOrders.push(customer);
    } else {
      return res.send("null")
    }
	
		return product.save().then(product => true).catch(err => err.message);
	})

	//checks if there are error in updating the course
	if(isProductUpdated !== true){
		return res.send({ message: isProductUpdated});
	}

	// Checkes if both user update and course update are successful.
	if(isUserUpdated && isProductUpdated){
		return res.send(true)
	}
}

// Getting user's ordered products
// module.exports.getEnrollments = (req, res) => {
// 	return User.findById(req.user.id).then(result => res.send(result.enrollments)).catch(err => res.send(err));
// }

// Function to reset the password
module.exports.resetPassword = async (req, res) => {
  try {

  	// used object desctructuring
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json({ message: 'Password reset successfully' });

  } catch (error) {

    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

module.exports.updateProfile = async (req, res) => {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNumber } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNumber },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Failed to update profile' });
  }
}
