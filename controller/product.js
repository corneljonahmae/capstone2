const Product = require("../models/Product")
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Product creation
module.exports.createProduct = (req, res) => {
  if (!req.user.isAdmin) {
    return Promise.reject("Only administrators can create products.");
  }

  const newProduct = new Product({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    isAdmin: req.body.isAdmin || false,
    imgURL: req.body.imgURL
  });

  return newProduct.save()
    .then(() => {
      return res.send(true); 
    })
    .catch((error) => {
      console.error(error);
      return res.send(false); 
    });
};

//Retrieve all products
module.exports.getAllProducts = (req,res) =>{
	return Product.find({}).then(result=>{
		return res.send(result)
	})
}

//Retrieve all active products
module.exports.getAllActiveProducts = (req,res) =>{
	return Product.find({isActive:true}).then(result=>{
		return res.send(result)
	})
}


//Retrieve single product
module.exports.getProduct = (req,res) =>{
	return Product.findById(req.params.productId).then(result=>{
		return res.send(result)
	})
};

// //Update Product information (Admin only)

module.exports.updateProduct = (req,res)=>{

	let updatedProduct = {
		name: req.body.name, 
        description: req.body.description,
        imgURL: req.body.imgURL,
        price: req.body.price,
        isActive: req.body.isActive, 
        purchasedOn: req.body.purchasedOn,
        userOrders: req.body.userOrders 
	}

	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error)=>{
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
}


//Archive Product (Admin only)
module.exports.archiveProduct = (req, res) => {
    Product.findById(req.params.productId).then(product => {
        if (product.isActive === true) {
            product.isActive = false;
            return product.save().then(updatedProduct => {
                if (updatedProduct) {
                    return res.send(true);
                } else {
                    return res.send(false);
                }
            });
        }
    }).catch(err => {
        console.error(err);
        return res.send(false);
    });
};

//Activate Product (Admin only)
module.exports.activateProduct = (req, res) => {
    Product.findById(req.params.productId).then(product => {
        if (product.isActive === false) {
            product.isActive = true;
            return product.save().then(updatedProduct => {
                if (updatedProduct) {
                    return res.send(true);
                } else {
                    return res.send(false);
                }
            });
        }
    }).catch(err => {
        console.error(err);
        return res.send(false);
    });
};


// Controller action to search for courses by course name
module.exports.searchProductByName = async (req, res) => {
    try {
  
      const { name } = req.body;
  
      // Use a regular expression to perform a case-insensitive search
      const products = await Product.find({
        name: { $regex: name, $options: 'i' }
      });
  
      res.json(products);
  
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  };
  
  module.exports.searchProductByPriceRange = async (req, res) => {
    try {
      const { minPrice, maxPrice } = req.body;
  
      // Find courses within the price range
      const products = await Product.find({
        price: { $gte: minPrice, $lte: maxPrice }
      });
  
      res.status(200).json({ products });
    } catch (error) {
      res.status(500).json({ error: 'An error occurred while searching for products' });
    }
  };
