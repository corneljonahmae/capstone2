const express = require("express");
const userController = require("../controller/user");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

const router = express.Router();

//Registration
router.post("/register", userController.registerUser);

//Authentication
router.post("/login", userController.loginUser);

// //Router to Retrieve User Details
router.post("/details", verify, userController.getProfile);

//Route to Non-admin User checkout (Create Order)
router.post("/checkout", verify, userController.createOrder);

// POST route for resetting the password
router.post('/reset-password', verify, userController.resetPassword);

// Update user profile route
router.put('/profile', verify, userController.updateProfile);


// Export Route System
module.exports = router;