const express = require("express");
const router = express.Router();
const productController = require("../controller/product")
const auth = require("../auth")
const {verify, verifyAdmin} = auth;


//Route for creating a product (admin only)
router.post("/create", verify, verifyAdmin, productController.createProduct);

//Route to get all products
router.get("/all", productController.getAllProducts);

//Route to get all active products
router.get("/",productController.getAllActiveProducts);

//Route to retrieve single product
router.get("/:productId",productController.getProduct);

//Route to update Product information (Admin only)
router.put("/product/:productId",verify, verifyAdmin, productController.updateProduct);

//Router to Archive Product (Admin only)
router.put("/:productId/archive",verify, verifyAdmin, productController.archiveProduct);

//Route to Activate Product (Admin only)
router.put("/:productId/activate",verify, verifyAdmin, productController.activateProduct);

// Route to search for product by course name
router.post('/search', productController.searchProductByName);

//Search Courses By Price Range
router.post('/searchByPrice', productController.searchProductByPriceRange);

// Export Route System
module.exports = router;

