const express = require("express");
const router = express.Router();
const productSliderController = require("../controller/productslider")
const auth = require("../auth")
const {verify, verifyAdmin} = auth;


//Route for creating a product (admin only)
router.post("/createslide", verify, verifyAdmin, productSliderController.createProductSlide);

//Route to get all products
router.get("/productslider/slides", productSliderController.getAllProductsSlide);

//Route to update Product information (Admin only)
router.put("/productslider/product/:slide-productId",verify, verifyAdmin, productSliderController.updateProductSlide);

//Router to Archive Product (Admin only)
router.put("/productslider/:productId/archive-slide",verify, verifyAdmin, productSliderController.archiveProductSlide);

//Route to Activate Product (Admin only)
router.put("/productslider/:productId/activate-slide",verify, verifyAdmin, productSliderController.activateProductSlide);


// Export Route System
module.exports = router;
