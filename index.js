// Create a simple Express JS application

// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
// Cors allows our backend application to be available to our frontend application

const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const slideRoutes = require("./routes/productslider");

// Environment Setup
const port = 4000;

// Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// Database Connection
	// Connect to our MongoDB Database
	mongoose.connect("mongodb+srv://corneljonahmae:admin123@batch-297.ev1567f.mongodb.net/Capstone2?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)

	// prompt
	let db = mongoose.connection;
	db.on('error', console.error.bind(console,'Connection error'));
	db.once('open',()=>console.log('Now Connected to MongoDB Atlas'));


	// [Backend Routes]
	// http://localhost:4000/user
	app.use("/users",userRoutes);
	app.use("/products",productRoutes);
	app.use("/productslider",slideRoutes);

	//To check if the middleware is working
	app.use((req, res, next) => {
	  console.log("Middleware is working!");
	  next();
	});


// Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, ()=>{
		console.log(`API is now online on port ${process.env.PORT || port}`)
	})
}


module.exports = {app,mongoose};