const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price:{
		type: Number,
		default: false
	},
	isActive:{
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	userOrders: [{
		userId: {
			type: String,
			// required: [true, "User order/s is required"]
		},
		orderId: {
			type: String,
			// required: true
		}
	}],
	imgURL: {
		type: String
	}
})

//module.exports = mongoose.model("Product", productSchema);


const Product = mongoose.model("Products", productSchema);

module.exports = Product;
