const mongoose = require("mongoose");

const productSliderSchema = new mongoose.Schema({
	imgURL: {
		type: String,
		required: [true, "Image is required"]
	}
})

const Slide = mongoose.model("ProductSlider", productSliderSchema);

module.exports = Slide;